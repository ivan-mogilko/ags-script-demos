
struct DungeonParty
{
  import int  GetX();
  import int  GetY();
  import CharacterDirection GetFace();
  import void SetPos(int x, int y, CharacterDirection face = eDirectionUp);
  import void Move(CharacterDirection dir);
  import void Face(CharacterDirection dir);
  
  bool AutoExplore;
  
  protected int _x, _y;
  protected CharacterDirection _face;
};

import DungeonParty Party;
