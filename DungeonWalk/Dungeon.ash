
enum DungeonCellType
{
  eCellInvalid = -1, 
  eCellWall = 0, 
  eCellPath
};

enum DungeonVisibility
{
  eVisHidden = 0, 
  eVisExplored
};

managed struct Cell
{
  int x, y;
};


struct Dungeon
{
  import void Create(int width, int height);
  
  import int  GetWidth();
  import int  GetHeight();
  import void FillCellType(DungeonCellType type);
  import DungeonCellType GetCell(int x, int y);
  import void SetCell(int x, int y, DungeonCellType type);
  
  import void FillVis(int vis);
  import int  GetVis(int x, int y);
  import void SetVis(int x, int y, int vis);
  
  import Cell *FindAnyCell(DungeonCellType type, int start_x, int start_y, int exclude[]);
  
  // objective description
  protected int _cell[];
  protected int _w;
  protected int _h;
  
  // observer description
  protected int _vis[];
};

import Dungeon ThisDungeon;
