
#define DUNVIEW_COLOR_BACKGROUND    0, 0, 0
#define DUNVIEW_COLOR_UNEXPLORED    0, 0, 0
#define DUNVIEW_COLOR_WALL          96, 96, 96
#define DUNVIEW_COLOR_PATH          192, 192, 192
#define DUNVIEW_COLOR_ERROR         255, 0, 0

#define DUNVIEW_COLOR_DEBUGFILL     128, 0, 0
#define DUNVIEW_COLOR_DEBUGFILL2    192, 0, 0
#define DUNVIEW_COLOR_DEBUGFILLTEXT 255, 255, 255
#define DUNVIEW_COLOR_GRID          128, 128, 128

#define DUNVIEW_COLOR_PARTY         4, 4, 255
#define DUNVIEW_COLOR_PARTY2        96, 4, 255


struct DungeonView
{
  import void SetViewport(int x, int y, int w, int h);
  import void SetScale(int cellw, int cellh);
  import void SetOrigin(int cellox, int celloy);
  import void DrawAll(DrawingSurface *ds);
  
  bool DebugFill;
  
  protected int _x, _y, _w, _h;
  protected int _cellw, _cellh;
  protected int _cellox, _celloy;
};

import DungeonView DV;
